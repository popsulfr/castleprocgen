#include "world.hpp"

#include <vector>

castleprocgen::World::World(int width,int height,int depth) 
: _world (width,std::vector<std::vector<BLOCK_ID>>(height,std::vector<BLOCK_ID>(depth,BLOCK_AIR))) {

}

castleprocgen::World::~World() {}

