#ifndef _RENDERER_HPP
#define _RENDERER_HPP

#include "sdl.hpp"

namespace castleprocgen {
class Renderer {
public:
	static const int TILE_WIDTH;
	Renderer ();
	virtual ~Renderer ();
	
	virtual void drawWorld();
	virtual void drawGrass();
	virtual void eventLoop();
	
protected:
	SDL _sdl;
	std::shared_ptr<SDL_Window> _window;
	std::shared_ptr<SDL_Renderer> _renderer;
};
}
#endif
