#ifndef _WORLD_HPP
#define _WORLD_HPP

#include <vector>

namespace castleprocgen {

typedef enum {
	BLOCK_TYPE_SURFACE = 0,
	BLOCK_TYPE_WALL,
	BLOCK_TYPE_MISC
} BLOCK_TYPE;

class WorldElement {
public:
	
protected:
	int _id;
	int _width;
	int _height;
	int _depth;
};
class Block {
	
};
class World {
public:
	typedef enum {
		BLOCK_AIR = 0,
		BLOCK_GRASS,
		BLOCK_STONE_WALL,
		
	} BLOCK_ID;
	
	World (int width,int height,int depth);
	virtual ~World ();
protected:
	std::vector<std::vector<std::vector<BLOCK_ID>>> _world;
};
}
#endif
