#include "renderer.hpp"

#include "sdl.hpp"

#include <SDL2/SDL_image.h>

#include <iostream>

const int castleprocgen::Renderer::TILE_WIDTH = 32;

castleprocgen::Renderer::Renderer() {
	this->_window = this->_sdl.CreateWindow(
		"Castle Procedural Generation",
		600,
		600);
	this->_renderer = this->_sdl.CreateRenderer(
		this->_window);
}

castleprocgen::Renderer::~Renderer() {}

void castleprocgen::Renderer::drawWorld() {
	SDL_SetRenderDrawColor(this->_renderer.get(),0,0,0,0);
	SDL_RenderClear(this->_renderer.get());
	
	std::unique_ptr<SDL_Surface,decltype(&SDL_FreeSurface)> ptr_surface(IMG_Load("wall-old.png"),&SDL_FreeSurface);
	if (!ptr_surface) {
		throw SDLException(SDL_GetError());
	}
	std::unique_ptr<SDL_Texture,decltype(&SDL_DestroyTexture)> ptr_texture(
		SDL_CreateTextureFromSurface(this->_renderer.get(),ptr_surface.get()),&SDL_DestroyTexture);
	
	int width,height;
	SDL_GetWindowSize(this->_window.get(),&width,&height);
	
	SDL_Rect dstrect;
	SDL_QueryTexture(ptr_texture.get(),nullptr,nullptr,&(dstrect.w),&(dstrect.h));
	
	int plus_width = dstrect.w/2;
	int plus_height = 54;//dstrect.h/2;
	int row = 0;
	int col = 0;
	
// 	for (dstrect.y = -plus_height; dstrect.y < height; dstrect.y += plus_height,++row) {
// 		for (dstrect.x = ((col%2)?0:-plus_width);dstrect.x < width; dstrect.x += dstrect.w,++col) {
// 			if ((row < 4) || (row > 8) || (col < 4) || (col > 8)) {
// 				//std::cout << "row: "
// 				SDL_RenderCopy(
// 					this->_renderer.get(),
// 					ptr_texture.get(),
// 					nullptr,
// 					&dstrect
// 				);
// 			}
// 		}
// 	}
	for (dstrect.y = (height-(plus_height<<2)); dstrect.y > -plus_height; dstrect.y -= plus_height,++row) {
		for (dstrect.x = ((col%2)?0:-plus_width);dstrect.x < width; dstrect.x += dstrect.w,++col) {
			//if ((row < 4) || (row > 8) || (col < 4) || (col > 8)) {
				//std::cout << "row: "
				SDL_RenderCopy(
					this->_renderer.get(),
					ptr_texture.get(),
					nullptr,
					&dstrect
				);
			//}
		}
	}
	SDL_RenderPresent(this->_renderer.get());
}

void castleprocgen::Renderer::drawGrass() {
	SDL_SetRenderDrawColor(this->_renderer.get(),0,0,0,0);
	SDL_RenderClear(this->_renderer.get());
	
	std::unique_ptr<SDL_Surface,decltype(&SDL_FreeSurface)> ptr_surface(IMG_Load("grass.png"),&SDL_FreeSurface);
	if (!ptr_surface) {
		throw SDLException(SDL_GetError());
	}
	std::unique_ptr<SDL_Texture,decltype(&SDL_DestroyTexture)> ptr_texture(
		SDL_CreateTextureFromSurface(this->_renderer.get(),ptr_surface.get()),&SDL_DestroyTexture);
	
	if (!ptr_texture) {
		throw SDLException(SDL_GetError());
	}
	
	int width,height;
	SDL_GetWindowSize(this->_window.get(),&width,&height);
	
	SDL_Rect dstrect;
	SDL_QueryTexture(ptr_texture.get(),nullptr,nullptr,&(dstrect.w),&(dstrect.h));
	
	int plus_width = dstrect.w/2;
	int plus_height = dstrect.h/2;
	int row = 0;
	int col = 0;
	
	for (dstrect.y = -plus_height; dstrect.y < height; dstrect.y += plus_height,++row) {
		for (dstrect.x = ((col%2)?0:-plus_width);dstrect.x < width; dstrect.x += dstrect.w,++col) {
			if ((row < 4) || (row > 8) || (col < 4) || (col > 8)) {
				//std::cout << "row: "
				SDL_RenderCopy(
					this->_renderer.get(),
					ptr_texture.get(),
					nullptr,
					&dstrect
				);
			}
		}
	}
	
	SDL_RenderPresent(this->_renderer.get());
}


void castleprocgen::Renderer::eventLoop() {
	while (true) {
		SDL_Event event;
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) {
			break;
		}
	}
}

