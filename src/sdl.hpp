#ifndef _SDL_HPP
#define _SDL_HPP

#include <SDL2/SDL.h>

#include <stdexcept>
#include <string>
#include <memory>

namespace castleprocgen {

class SDLException : public std::runtime_error {
public:
	SDLException(const std::string& message);
	virtual ~SDLException();
};

class SDL {
public:
	SDL ();
	virtual ~SDL ();
	
	/**
	 * See SDL_CreateWindow() doc
	 * @param x SDL_WINDOWPOS_CENTERED or SDL_WINDOWPOS_UNDEFINED
	 * @param y SDL_WINDOWPOS_CENTERED or SDL_WINDOWPOS_UNDEFINED
	 * @param flags OR'd
	 * 	SDL_WINDOW_FULLSCREEN
	 * 	SDL_WINDOW_FULLSCREEN_DESKTOP
	 * 	SDL_WINDOW_OPENGL
	 * 	SDL_WINDOW_HIDDEN
	 * 	SDL_WINDOW_BORDERLESS
	 * 	SDL_WINDOW_RESIZABLE
	 * 	SDL_WINDOW_MINIMIZED
	 * 	SDL_WINDOW_MAXIMIZED
	 * 	SDL_WINDOW_INPUT_GRABBED
	 * 	SDL_WINDOW_ALLOW_HIGHDPI
	 */
	virtual std::shared_ptr<SDL_Window> CreateWindow(
		const std::string title,
		int x,int y,int w, int h,Uint32 flags=0) const;
	virtual std::shared_ptr<SDL_Window> CreateWindow(
		const std::string title,
		int w, int h,Uint32 flags=0) const {
		return this->CreateWindow(
			title,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,w,h,flags);
	}
	
	/**
	 * See SDL_CreateRenderer() doc
	 * @param flags OR'd
	 * 	SDL_RENDERER_SOFTWARE
	 * 	SDL_RENDERER_ACCELERATED
	 * 	SDL_RENDERER_PRESENTVSYNC
	 * 	SDL_RENDERER_TARGETTEXTURE
	 */
	virtual std::shared_ptr<SDL_Renderer> CreateRenderer(
		std::shared_ptr<SDL_Window> window,
		int index,
		Uint32 flags=SDL_RENDERER_ACCELERATED) const;
	virtual std::shared_ptr<SDL_Renderer> CreateRenderer(
		std::shared_ptr<SDL_Window> window,
		Uint32 flags=SDL_RENDERER_ACCELERATED) const {
		return this->CreateRenderer(window,-1,flags);
	}
	
};
}
#endif
