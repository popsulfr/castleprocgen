#include "sdl.hpp"

#include <SDL2/SDL.h>

#include <stdexcept>
#include <iostream>
#include <memory>

castleprocgen::SDLException::SDLException(const std::string& message) 
: std::runtime_error(message) {}

castleprocgen::SDLException::~SDLException() {}

castleprocgen::SDL::SDL () {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		throw SDLException(SDL_GetError());
	}
	SDL_version vcompiled,vlinked;
	SDL_VERSION(&vcompiled);
	SDL_GetVersion(&vlinked);
	SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION,
		    "Compiled SDL version : %d.%d.%d\n",
		    vcompiled.major,vcompiled.minor,vcompiled.patch
	);
	SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION,
		    "Linked SDL version : %d.%d.%d\n",
		    vlinked.major,vlinked.minor,vlinked.patch
	);
}

castleprocgen::SDL::~SDL() {
	SDL_Quit();
}

std::shared_ptr< SDL_Window > castleprocgen::SDL::CreateWindow(
	const std::string title,
	int x, int y, int w, int h, Uint32 flags) const {
	std::shared_ptr<SDL_Window> window(
		SDL_CreateWindow(title.c_str(),x,y,w,h,flags),
		&SDL_DestroyWindow);
	if (!window) {
		throw SDLException(SDL_GetError());
	}
	return window;
}

std::shared_ptr<SDL_Renderer> castleprocgen::SDL::CreateRenderer(
	std::shared_ptr<SDL_Window> window,
	int index,
	Uint32 flags) const {
	std::shared_ptr<SDL_Renderer> renderer(
		SDL_CreateRenderer(window.get(),index,flags),
		&SDL_DestroyRenderer);
	if (!renderer) {
		throw SDLException(SDL_GetError());
	}
	return renderer;
}


